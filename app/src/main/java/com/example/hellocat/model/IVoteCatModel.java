package com.example.hellocat.model;

public interface IVoteCatModel {

    public void getCat();

    public void voteCat(VoteDataModel model);

    public void nopeCat(VoteDataModel model);

}
