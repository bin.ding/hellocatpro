package com.example.hellocat.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.hellocat.R;
import com.example.hellocat.ui.widget.FavouriteView;

public class ViewHolder extends RecyclerView.ViewHolder {

    public FavouriteView mView;

    public ViewHolder(View view) {
        super(view);
        mView = (FavouriteView) view;
    }
}
