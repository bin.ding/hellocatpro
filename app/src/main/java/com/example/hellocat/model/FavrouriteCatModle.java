package com.example.hellocat.model;

import android.text.TextUtils;

import com.example.hellocat.service.dao.AddressManagerConst;
import com.example.hellocat.service.log.LogUtils;
import com.example.hellocat.service.network.NetworkUtils;
import com.example.hellocat.service.notify.ToastUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FavrouriteCatModle implements IFavouriteModel{
    private static final String TAG = FavrouriteCatModle.class.getSimpleName();

    private onFinish mFinish;

    public FavrouriteCatModle(onFinish onFinish) {
        mFinish = onFinish;
    }

    public interface onFinish {
        public void onGetMoreFinish(ArrayList<FavrouiteModel> imgList);
        public void favouriteFinshi(String url, String favId, String subId);
        public void onGetFavFinish();
        public void onDeleteFav(String msg);
    }

    @Override
    public void getFavrouriteKitties() {

    }

    @Override
    public void getMoreKitties() {
        ArrayList<FavrouiteModel> catList = getMoreCats();
        mFinish.onGetMoreFinish(catList);
    }

    @Override
    public void favrouriteIt(FavrouiteModel model) {
        NetworkUtils.NetworkModel data = new NetworkUtils.NetworkModel();
        String url = AddressManagerConst.FAVROURITE_KITTEY_ADDR;
        JSONObject obj = new JSONObject();
        try {
            obj.put("image_id", model.mImgId);
            obj.put("sub_id", model.mSubId);
            data.json = obj.toString();
            NetworkUtils.getDataSync(url,
                    data);
            String result = data.result;
            LogUtils.d(TAG, "[favrouriteIt] result is " + result);

            if (!TextUtils.isEmpty(result)) {
                JSONObject dataObj = new JSONObject(result);
                String msg = dataObj.optString("message");
                if (data.ret == 0) {
                    if ("SUCCESS".equals(msg)) {
                        String favId = dataObj.optString("id");
                        mFinish.favouriteFinshi(model.url, favId, model.mSubId);
                    }
                } else {
                    ToastUtil.showToast(msg);
                }

            }
        } catch (JSONException e) {
            LogUtils.d(TAG, e.toString());
        }
    }

    private ArrayList<FavrouiteModel> getMoreCats() {
        ArrayList<FavrouiteModel> list = new ArrayList<FavrouiteModel>();
        NetworkUtils.NetworkModel model = new NetworkUtils.NetworkModel();
        NetworkUtils.getDataSync(AddressManagerConst.MORE_KITTIES_ADDR, model);
        if (model != null && !TextUtils.isEmpty(model.result) && model.ret == 0) {
            try {
                LogUtils.d(TAG, "[getMoreCats] " + model.result);
                JSONArray array = new JSONArray(model.result);
                if (array != null && array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        String id = obj.optString("id");
                        String imgUrl = obj.optString("url");
                        FavrouiteModel m = new FavrouiteModel();
                        m.url = imgUrl;
                        m.mSubId = "dingbin";
                        m.mImgId = id;
                        list.add(m);
                    }
                }
            } catch (JSONException e) {
                LogUtils.e(TAG, e.toString());
            }
        }
        return list;
    }


    @Override
    public void deleteFavrouite(FavrouiteModel model) {
        NetworkUtils.doDelete(AddressManagerConst.FAVROUITE_DELETE_ADDR + model.mFavId, new NetworkUtils.INetworkCallback() {
            @Override
            public void onResponse(int ret, String result, Object obj) {
                LogUtils.d(TAG, "[deleteFavrouite] FavDel " + ret + ",," + result);
                mFinish.onDeleteFav("Delete " + result);
            }
        });
    }
}
