package com.example.hellocat.model;

import android.text.TextUtils;

import com.example.hellocat.service.cache.CacheManager;
import com.example.hellocat.service.dao.AddressManagerConst;
import com.example.hellocat.service.json.JsonUtils;
import com.example.hellocat.service.log.LogUtils;
import com.example.hellocat.service.network.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

public class CatBreedsModel implements IBreedsModel{

    private static final String TAG = CatBreedsModel.class.getSimpleName();

    ArrayList<Cat> mCatList;

    private OnFinish mOnFinish;

    public CatBreedsModel(OnFinish onFinish) {
        this.mOnFinish = onFinish;
    }

    public interface OnFinish {
        void onFinishListener(ArrayList<Cat> lists);

        void onFetchFinish(List<Cat> imgList, String desp, String wikiUrl);
    }

    @Override
    public void getData() {
        mCatList = new ArrayList<Cat>();
        NetworkUtils.sendRequestWithOkHttp(AddressManagerConst
                .CAT_BREEDS_LIST_ADDR, new NetworkUtils.INetworkCallback() {
            @Override
            public void onResponse(int ret, String result, Object obj) {
                LogUtils.d(TAG, "Breeds onResonse " + ret);
                if (ret == 0) {
                    mCatList = JsonUtils.tranformToCatList(result);
                    new CacheManager().cacheCatBreedsData(mCatList);
                    mOnFinish.onFinishListener(mCatList);
                } else {
                    mCatList = new CacheManager().getCacheBreeds();
                    mOnFinish.onFinishListener(mCatList);
                }
            }
        });
    }

    private ArrayList<Cat> cacheCatList = null;

    @Override
    public void getSelectData(String name) {
        LogUtils.d(TAG, "Breeds getSelected Data " + name);
        cacheCatList = new ArrayList<Cat>();
        NetworkUtils.NetworkModel model = new NetworkUtils.NetworkModel();
        NetworkUtils.getDataSync(AddressManagerConst.CAT_BREEDS_IMG_ADDR + name
                + "&limit=5", model);
        ArrayList<Cat> firstList = null;
        if(model != null && !TextUtils.isEmpty(model.result) && model.ret == 0) {
            firstList = JsonUtils.tranformToCatList(model.result);
            if (firstList != null && firstList.size() > 0) {
                for (Cat c : firstList) {
                    cacheCatList.add(c);
                    LogUtils.d(TAG, "[getSelectData] " + c.getUrl());
                }
            }
        }


        NetworkUtils.sendRequestWithOkHttp(AddressManagerConst.CAT_BREEDS_IMG_ADDR + name,
                new NetworkUtils.INetworkCallback() {
                    @Override
                    public void onResponse(int ret, String result, Object obj) {
                        if (ret == 0) {
                            ArrayList<Cat> list = new ArrayList<Cat>();
                            list = JsonUtils.tranformToCatList(result);
                            if (list != null && list.size() > 0) {
                                List<Cat> cacheList = new CacheManager().getCatByName(name);
                                String desp = null;
                                String wikiUrl = null;
                                if (cacheList != null && cacheList.size() > 0) {
                                    Cat cacheCat = cacheList.get(0);
                                    desp = cacheCat.getDescription();
                                    wikiUrl = cacheCat.getWikipediaUrl();
                                }
                                if (cacheCatList != null) {
                                    list.addAll(cacheCatList);
                                }
                                mOnFinish.onFetchFinish(list, desp, wikiUrl);
                            } else {
                                // TODO
                            }
                        } else {
                            String cacheUrl = new CacheManager().getCacheUrl(name);
                            List<Cat> cacheList = new CacheManager().getCatByName(name);
                            if (cacheList != null && cacheList.size() > 0) {
                                Cat cat = cacheList.get(0);
                                mOnFinish.onFetchFinish(null, cat.getDescription(), cat.getWikipediaUrl());
                            }
                        }
                    }
                });
    }
}
