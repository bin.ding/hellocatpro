package com.example.hellocat.model;

public interface IBreedsModel {

    void getData();

    void getSelectData(String name);
}
