package com.example.hellocat.model;

import android.text.TextUtils;

import com.example.hellocat.service.dao.AddressManagerConst;
import com.example.hellocat.service.log.LogUtils;
import com.example.hellocat.service.network.NetworkUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VoteCatModel implements IVoteCatModel{

    private static final String TAG = VoteCatModel.class.getSimpleName();
    private onFinish mOnFinish;

    public VoteCatModel(onFinish onFinish) {
        mOnFinish = onFinish;
    }

    public interface onFinish {
        public void onUpdateCat(VoteDataModel model);
        public void onUpdateArea(ArrayList<VoteDataModel> data);
        public void showErrerMsg(String msg);
    }

    @Override
    public void getCat() {
        NetworkUtils.NetworkModel model = new NetworkUtils.NetworkModel();
        NetworkUtils.getDataSync(AddressManagerConst.MORE_KITTIES_ADDR, model);
        VoteDataModel m = new VoteDataModel();
        if (model != null && !TextUtils.isEmpty(model.result) && model.ret == 0) {
            try {
                LogUtils.d(TAG, "[getCat] " + model.result);
                JSONArray array = new JSONArray(model.result);
                if (array != null && array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        String id = obj.optString("id");
                        String imgUrl = obj.optString("url");
                        m.url = imgUrl;
                        m.mSubId = "dingbin";
                        m.mImgId = id;
                        break;
                    }
                }
            } catch (JSONException e) {
                LogUtils.e(TAG, e.toString());
            }
        }

        if (!TextUtils.isEmpty(m.url)) {
            mOnFinish.onUpdateCat(m);
        }
    }

    @Override
    public void nopeCat(VoteDataModel model) {
        solveVote(model, 0);
    }

    @Override
    public void voteCat(VoteDataModel model) {
        solveVote(model, 1);
    }

    public void solveVote(VoteDataModel model, int value) {
        NetworkUtils.NetworkModel data = new NetworkUtils.NetworkModel();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("image_id", model.mImgId);
            jsonObject.put("sub_id", model.mSubId);
            jsonObject.put("value", value);
            data.json = jsonObject.toString();
        } catch (JSONException t){
            LogUtils.d(TAG, t.toString());
            mOnFinish.showErrerMsg(t.toString());
            return;
        }

        NetworkUtils.getDataSync(AddressManagerConst.VOTE_CAT_ADDR, data);
        LogUtils.d(TAG, "[voteCat] " + data.ret + "，，" + data.result);
        if (data.ret == 0) {
            try {
                JSONObject obj = new JSONObject(data.result);
                String tag = obj.optString("message");
                if ("SUCCESS".equals(tag)) {
                    getCat();
                    getVotedCats();
                }  else {
                    mOnFinish.showErrerMsg(data.result);
                }
            } catch (JSONException e) {
                LogUtils.d(TAG, e.toString());
            }
        } else {
            mOnFinish.showErrerMsg(data.result);
        }
    }

    private void getVotedCats() {
        NetworkUtils.NetworkModel model = new NetworkUtils.NetworkModel();
        ArrayList<VoteDataModel> list = new ArrayList<VoteDataModel>();
        NetworkUtils.getDataSync(AddressManagerConst.VOTED_GET_ADDR + "sub_id=dingbin", model);
        if (model.ret == 0 && !TextUtils.isEmpty(model.result)) {
            try {
                JSONArray jsonArray = new JSONArray(model.result);
                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    VoteDataModel cat = new VoteDataModel();
                    cat.mImgId = object.optString("image_id");
                    cat.mSubId = object.optString("sub_id");
                    cat.mCreateTime = object.optString("created_at");
                    cat.mValue = object.optString("value");
                    list.add(cat);
                }
            } catch (JSONException e) {
                LogUtils.d(TAG, e.toString());
            }
        }
        mOnFinish.onUpdateArea(list);
    }
}
