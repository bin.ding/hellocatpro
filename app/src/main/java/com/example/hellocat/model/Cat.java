
package com.example.hellocat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;

@Entity
public class Cat {

    @SerializedName("adaptability")
    @Expose
    public Integer adaptability;

    public String url;

    @SerializedName("affection_level")
    @Expose
    public Integer affectionLevel;
    @SerializedName("alt_names")
    @Expose
    public String altNames;
    @SerializedName("cfa_url")
    @Expose
    public String cfaUrl;
    @SerializedName("child_friendly")
    @Expose
    public Integer childFriendly;
    @SerializedName("country_code")
    @Expose
    public String countryCode;
    @SerializedName("country_codes")
    @Expose
    public String countryCodes;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("dog_friendly")
    @Expose
    public Integer dogFriendly;
    @SerializedName("energy_level")
    @Expose
    public Integer energyLevel;
    @SerializedName("experimental")
    @Expose
    public Integer experimental;
    @SerializedName("grooming")
    @Expose
    public Integer grooming;
    @SerializedName("hairless")
    @Expose
    public Integer hairless;
    @SerializedName("health_issues")
    @Expose
    public Integer healthIssues;
    @SerializedName("hypoallergenic")
    @Expose
    public Integer hypoallergenic;
    @SerializedName("id")
    @Expose
    @Unique
    public String id;
    @SerializedName("indoor")
    @Expose
    public Integer indoor;
    @SerializedName("intelligence")
    @Expose
    public Integer intelligence;
    @SerializedName("lap")
    @Expose
    public Integer lap;
    @SerializedName("life_span")
    @Expose
    public String lifeSpan;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("natural")
    @Expose
    public Integer natural;
    @SerializedName("origin")
    @Expose
    public String origin;
    @SerializedName("rare")
    @Expose
    public Integer rare;
    @SerializedName("rex")
    @Expose
    public Integer rex;
    @SerializedName("shedding_level")
    @Expose
    public Integer sheddingLevel;
    @SerializedName("short_legs")
    @Expose
    public Integer shortLegs;
    @SerializedName("social_needs")
    @Expose
    public Integer socialNeeds;
    @SerializedName("stranger_friendly")
    @Expose
    public Integer strangerFriendly;
    @SerializedName("suppressed_tail")
    @Expose
    public Integer suppressedTail;
    @SerializedName("temperament")
    @Expose
    public String temperament;
    @SerializedName("vcahospitals_url")
    @Expose
    public String vcahospitalsUrl;
    @SerializedName("vetstreet_url")
    @Expose
    public String vetstreetUrl;
    @SerializedName("vocalisation")
    @Expose
    public Integer vocalisation;

    @SerializedName("wikipedia_url")
    @Expose
    public String wikipediaUrl;

    @Generated(hash = 2093037906)
    public Cat(Integer adaptability, String url, Integer affectionLevel,
            String altNames, String cfaUrl, Integer childFriendly,
            String countryCode, String countryCodes, String description,
            Integer dogFriendly, Integer energyLevel, Integer experimental,
            Integer grooming, Integer hairless, Integer healthIssues,
            Integer hypoallergenic, String id, Integer indoor, Integer intelligence,
            Integer lap, String lifeSpan, String name, Integer natural,
            String origin, Integer rare, Integer rex, Integer sheddingLevel,
            Integer shortLegs, Integer socialNeeds, Integer strangerFriendly,
            Integer suppressedTail, String temperament, String vcahospitalsUrl,
            String vetstreetUrl, Integer vocalisation, String wikipediaUrl) {
        this.adaptability = adaptability;
        this.url = url;
        this.affectionLevel = affectionLevel;
        this.altNames = altNames;
        this.cfaUrl = cfaUrl;
        this.childFriendly = childFriendly;
        this.countryCode = countryCode;
        this.countryCodes = countryCodes;
        this.description = description;
        this.dogFriendly = dogFriendly;
        this.energyLevel = energyLevel;
        this.experimental = experimental;
        this.grooming = grooming;
        this.hairless = hairless;
        this.healthIssues = healthIssues;
        this.hypoallergenic = hypoallergenic;
        this.id = id;
        this.indoor = indoor;
        this.intelligence = intelligence;
        this.lap = lap;
        this.lifeSpan = lifeSpan;
        this.name = name;
        this.natural = natural;
        this.origin = origin;
        this.rare = rare;
        this.rex = rex;
        this.sheddingLevel = sheddingLevel;
        this.shortLegs = shortLegs;
        this.socialNeeds = socialNeeds;
        this.strangerFriendly = strangerFriendly;
        this.suppressedTail = suppressedTail;
        this.temperament = temperament;
        this.vcahospitalsUrl = vcahospitalsUrl;
        this.vetstreetUrl = vetstreetUrl;
        this.vocalisation = vocalisation;
        this.wikipediaUrl = wikipediaUrl;
    }

    @Generated(hash = 205319056)
    public Cat() {
    }

    public Integer getAdaptability() {
        return this.adaptability;
    }

    public void setAdaptability(Integer adaptability) {
        this.adaptability = adaptability;
    }

    public Integer getAffectionLevel() {
        return this.affectionLevel;
    }

    public void setAffectionLevel(Integer affectionLevel) {
        this.affectionLevel = affectionLevel;
    }

    public String getAltNames() {
        return this.altNames;
    }

    public void setAltNames(String altNames) {
        this.altNames = altNames;
    }

    public String getCfaUrl() {
        return this.cfaUrl;
    }

    public void setCfaUrl(String cfaUrl) {
        this.cfaUrl = cfaUrl;
    }

    public Integer getChildFriendly() {
        return this.childFriendly;
    }

    public void setChildFriendly(Integer childFriendly) {
        this.childFriendly = childFriendly;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCodes() {
        return this.countryCodes;
    }

    public void setCountryCodes(String countryCodes) {
        this.countryCodes = countryCodes;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDogFriendly() {
        return this.dogFriendly;
    }

    public void setDogFriendly(Integer dogFriendly) {
        this.dogFriendly = dogFriendly;
    }

    public Integer getEnergyLevel() {
        return this.energyLevel;
    }

    public void setEnergyLevel(Integer energyLevel) {
        this.energyLevel = energyLevel;
    }

    public Integer getExperimental() {
        return this.experimental;
    }

    public void setExperimental(Integer experimental) {
        this.experimental = experimental;
    }

    public Integer getGrooming() {
        return this.grooming;
    }

    public void setGrooming(Integer grooming) {
        this.grooming = grooming;
    }

    public Integer getHairless() {
        return this.hairless;
    }

    public void setHairless(Integer hairless) {
        this.hairless = hairless;
    }

    public Integer getHealthIssues() {
        return this.healthIssues;
    }

    public void setHealthIssues(Integer healthIssues) {
        this.healthIssues = healthIssues;
    }

    public Integer getHypoallergenic() {
        return this.hypoallergenic;
    }

    public void setHypoallergenic(Integer hypoallergenic) {
        this.hypoallergenic = hypoallergenic;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIndoor() {
        return this.indoor;
    }

    public void setIndoor(Integer indoor) {
        this.indoor = indoor;
    }

    public Integer getIntelligence() {
        return this.intelligence;
    }

    public void setIntelligence(Integer intelligence) {
        this.intelligence = intelligence;
    }

    public Integer getLap() {
        return this.lap;
    }

    public void setLap(Integer lap) {
        this.lap = lap;
    }

    public String getLifeSpan() {
        return this.lifeSpan;
    }

    public void setLifeSpan(String lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNatural() {
        return this.natural;
    }

    public void setNatural(Integer natural) {
        this.natural = natural;
    }

    public String getOrigin() {
        return this.origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Integer getRare() {
        return this.rare;
    }

    public void setRare(Integer rare) {
        this.rare = rare;
    }

    public Integer getRex() {
        return this.rex;
    }

    public void setRex(Integer rex) {
        this.rex = rex;
    }

    public Integer getSheddingLevel() {
        return this.sheddingLevel;
    }

    public void setSheddingLevel(Integer sheddingLevel) {
        this.sheddingLevel = sheddingLevel;
    }

    public Integer getShortLegs() {
        return this.shortLegs;
    }

    public void setShortLegs(Integer shortLegs) {
        this.shortLegs = shortLegs;
    }

    public Integer getSocialNeeds() {
        return this.socialNeeds;
    }

    public void setSocialNeeds(Integer socialNeeds) {
        this.socialNeeds = socialNeeds;
    }

    public Integer getStrangerFriendly() {
        return this.strangerFriendly;
    }

    public void setStrangerFriendly(Integer strangerFriendly) {
        this.strangerFriendly = strangerFriendly;
    }

    public Integer getSuppressedTail() {
        return this.suppressedTail;
    }

    public void setSuppressedTail(Integer suppressedTail) {
        this.suppressedTail = suppressedTail;
    }

    public String getTemperament() {
        return this.temperament;
    }

    public void setTemperament(String temperament) {
        this.temperament = temperament;
    }

    public String getVcahospitalsUrl() {
        return this.vcahospitalsUrl;
    }

    public void setVcahospitalsUrl(String vcahospitalsUrl) {
        this.vcahospitalsUrl = vcahospitalsUrl;
    }

    public String getVetstreetUrl() {
        return this.vetstreetUrl;
    }

    public void setVetstreetUrl(String vetstreetUrl) {
        this.vetstreetUrl = vetstreetUrl;
    }

    public Integer getVocalisation() {
        return this.vocalisation;
    }

    public void setVocalisation(Integer vocalisation) {
        this.vocalisation = vocalisation;
    }

    public String getWikipediaUrl() {
        return this.wikipediaUrl;
    }

    public void setWikipediaUrl(String wikipediaUrl) {
        this.wikipediaUrl = wikipediaUrl;
    }


    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
