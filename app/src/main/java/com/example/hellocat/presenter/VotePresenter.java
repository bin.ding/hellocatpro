package com.example.hellocat.presenter;

import com.example.hellocat.model.VoteCatModel;
import com.example.hellocat.model.VoteDataModel;
import com.example.hellocat.ui.view.IVoteView;

import java.util.ArrayList;

public class VotePresenter implements VoteCatModel.onFinish {

    private IVoteView mView;

    private VoteCatModel mVoteModel;

    public VotePresenter(IVoteView view) {
        mView = view;
        mVoteModel = new VoteCatModel(this);
    }

    @Override
    public void onUpdateArea(ArrayList<VoteDataModel> data) {
        mView.updateVoteArea(data);
    }

    @Override
    public void onUpdateCat(VoteDataModel model) {
        mView.updateCat(model);
    }

    public void voteCat(VoteDataModel model) {
        mVoteModel.voteCat(model);
    }

    public void nopeCat(VoteDataModel model) {
        mVoteModel.nopeCat(model);
    }

    public void getCat() {
        mVoteModel.getCat();
    }

    @Override
    public void showErrerMsg(String msg) {
        mView.showErrorToast(msg);
    }
}
