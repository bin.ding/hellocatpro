package com.example.hellocat.presenter;

import com.example.hellocat.model.Cat;
import com.example.hellocat.model.CatBreedsModel;
import com.example.hellocat.service.log.LogUtils;
import com.example.hellocat.ui.view.IBreedsView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CatBreedsPresenter implements CatBreedsModel.OnFinish {

    private static final String TAG = CatBreedsPresenter.class.getSimpleName();

    private IBreedsView mView;
    private CatBreedsModel mCatModel;

    private ArrayList<String> mSpList;

    public CatBreedsPresenter(IBreedsView iView) {
        mCatModel = new CatBreedsModel(this);
        mView = iView;
    }

    @Override
    public void onFinishListener(ArrayList<Cat> lists) {
        LogUtils.d(TAG, "Breeds onFinishListener enter");
        // update view;
        if (lists != null && lists.size() > 0) {
            int size = lists.size();
            mSpList = new ArrayList<String>();
            for (Cat cat : lists) {
                mSpList.add(cat.getName());
            }
            Cat cat = lists.get(0);
            mView.updateSpList(mSpList, cat.getDescription(), cat.getWikipediaUrl());
            mCatModel.getSelectData(cat.name);
        }
    }

    public void getData() {
        mCatModel.getData();
    }

    public void getSelectData(int index) {
        String name = mSpList.get(index);
        LogUtils.d(TAG, "[getSelectData] name = " + name);
        mCatModel.getSelectData(name);
    }

    @Override
    public void onFetchFinish(List<Cat> catList, String desp, String wikiUrl) {
        ArrayList<String> imgList = new ArrayList<String>();
        if (catList != null && catList.size() > 0) {
            for (Cat cat : catList) {
                imgList.add(cat.getUrl());
            }
        }
        mView.updateCatInfo(imgList, desp, wikiUrl);
    }
}
