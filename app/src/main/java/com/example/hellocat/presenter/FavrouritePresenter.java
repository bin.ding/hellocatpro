package com.example.hellocat.presenter;

import com.example.hellocat.model.FavrouiteModel;
import com.example.hellocat.model.FavrouriteCatModle;
import com.example.hellocat.ui.view.IFavrouteView;

import java.util.ArrayList;

public class FavrouritePresenter implements FavrouriteCatModle.onFinish {

    private IFavrouteView mView;
    private FavrouriteCatModle mFavModel;

    public FavrouritePresenter(IFavrouteView view) {
        mView = view;
        mFavModel = new FavrouriteCatModle(this);
    }

    @Override
    public void onGetFavFinish() {

    }

    @Override
    public void onGetMoreFinish(ArrayList<FavrouiteModel> list) {
        mView.onImgUpdate(list);
    }

    @Override
    public void favouriteFinshi(String url, String favId, String subId) {
        mView.onFavViewUpdate(url, favId, subId);
    }

    public void getFavKitties() {

    }

    public void favrouiteIt(FavrouiteModel model) {
        if (model == null) {
            return;
        }
        mFavModel.favrouriteIt(model);
    }

    public void getMoreKitties() {
        mFavModel.getMoreKitties();
    }

    public void deleteFavrouite(FavrouiteModel model) {
        mFavModel.deleteFavrouite(model);
    }

    @Override
    public void onDeleteFav(String msg) {
        mView.showDeleteMsg(msg);
    }
}
