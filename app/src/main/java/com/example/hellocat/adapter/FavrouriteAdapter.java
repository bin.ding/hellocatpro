package com.example.hellocat.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.hellocat.model.FavrouiteModel;
import com.example.hellocat.model.FavrouriteCatModle;
import com.example.hellocat.model.ViewHolder;
import com.example.hellocat.presenter.FavrouritePresenter;
import com.example.hellocat.ui.widget.FavouriteView;

import java.util.ArrayList;
import java.util.List;

public class FavrouriteAdapter extends RecyclerView.Adapter<ViewHolder> {


    private Context mContext;
    private ArrayList<FavrouiteModel> mDatalist;
    private FavrouritePresenter mPresenter;
    public FavrouriteAdapter(Context context, ArrayList<FavrouiteModel> list, FavrouritePresenter presenter) {
        mContext = context;
        mDatalist = list;
        mPresenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder holder = new ViewHolder(new FavouriteView(mContext));
        holder.mView.mPresenter = mPresenter;
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (mDatalist != null && mDatalist.size() > position) {
            FavrouiteModel model = mDatalist.get(position);
            holder.mView.updateView(model);
        }
    }

    @Override
    public int getItemCount() {
        return mDatalist == null ? 0 : mDatalist.size();
    }
}
