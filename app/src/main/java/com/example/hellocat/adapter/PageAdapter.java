package com.example.hellocat.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.hellocat.service.log.LogUtils;
import com.example.hellocat.ui.FavrouriteFragment;
import com.example.hellocat.ui.VoteFragment;

import java.util.HashMap;

public class PageAdapter extends FragmentPagerAdapter {
    private static final String TAG = PageAdapter.class.getSimpleName();

    private int mNumber;
    private HashMap<Integer, Fragment> mFragmentMap = new HashMap<Integer, Fragment>();

    public PageAdapter(FragmentManager fm, int num) {
        super(fm);
        mNumber = num;
    }

    @Override
    public Fragment getItem(int pos) {
        Fragment fragment = mFragmentMap.get(pos);
        if (fragment == null) {
            switch (pos) {
                case 0: {
                    fragment = new VoteFragment();
                    LogUtils.d(TAG, "[getItem] vote fragment " );
                    break;
                }
                case 1:{
                    fragment = new FavrouriteFragment();
                    LogUtils.d(TAG, "[getItem] favroute fragment " );
                    break;
                }
                default:
                    break;
            }
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return mNumber;
    }
}
