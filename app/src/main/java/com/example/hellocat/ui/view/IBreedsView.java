package com.example.hellocat.ui.view;

import java.util.List;

public interface IBreedsView {

    /**
     * 更新sp列表
     * @param nameList
     */
    void updateSpList(List<String> nameList, String desp, String wikiUrl);

    /**
     * 随机选取一只小猫更新
     */
    void updateCatInfo(List<String> imgUrlList, String desp, String wikiUrl);
}
