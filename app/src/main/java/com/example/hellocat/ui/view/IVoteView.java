package com.example.hellocat.ui.view;

import com.example.hellocat.model.VoteCatModel;
import com.example.hellocat.model.VoteDataModel;

import java.util.ArrayList;

public interface IVoteView {

    public void updateCat(VoteDataModel model);

    public void updateVoteArea(ArrayList<VoteDataModel> catModels);

    public void showErrorToast(String msg);
}
