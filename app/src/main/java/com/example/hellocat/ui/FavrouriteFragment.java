package com.example.hellocat.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hellocat.R;
import com.example.hellocat.adapter.FavrouriteAdapter;
import com.example.hellocat.model.FavrouiteModel;
import com.example.hellocat.presenter.FavrouritePresenter;
import com.example.hellocat.service.notify.ToastUtil;
import com.example.hellocat.ui.view.IFavrouteView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FavrouriteFragment extends BaseFragment implements IFavrouteView {

    private RecyclerView mRecycleView;
    private RecyclerView.LayoutManager mLayoutManager;
    private FavrouriteAdapter mAdapter;
    private FavrouritePresenter mFavPresenter;
    private Button mBtnMore;

    private ImageView mIconFav;
    private TextView mTextFavId;
    private TextView mTextSubId;
    private TextView mBtnDelete;

    private Handler mHadler = new Handler(Looper.myLooper());

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_favourite_layout, container, false);
        mRecycleView = root.findViewById(R.id.recycle_view);
        mBtnMore = (Button) root.findViewById(R.id.btn_more_cat);
        mIconFav = (ImageView) root.findViewById(R.id.icon_fav_result);
        mTextFavId = (TextView) root.findViewById(R.id.text_fav_id);
        mTextSubId = (TextView) root.findViewById(R.id.text_sub_id);
        mBtnDelete = (Button) root.findViewById(R.id.btn_delete);

        mLayoutManager = new GridLayoutManager(getContext(), 3);
        mRecycleView.setLayoutManager(mLayoutManager);
        mRecycleView.setHasFixedSize(true);

        // test
//        ArrayList<String> list = new ArrayList<String>();
//        list.add("https://cdn2.thecatapi.com/images/o0.gif");
//        list.add("https://cdn2.thecatapi.com/images/2g0.jpg");
//        list.add("https://cdn2.thecatapi.com/images/280.jpg");
//        mAdapter = new FavrouriteAdapter(getContext(), list);
//        mRecycleView.setAdapter(mAdapter);
        mFavPresenter = new FavrouritePresenter(this);
        mBtnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFavPresenter.getMoreKitties();
            }
        });
        mBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavrouiteModel model = new FavrouiteModel();
                model.mFavId = mTextFavId.getText().toString().trim();
                mFavPresenter.deleteFavrouite(model);
            }
        });
        mBtnDelete.setVisibility(View.INVISIBLE);
        return root;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onImgUpdate(ArrayList<FavrouiteModel> modelsList) {
        mHadler.post(new Runnable() {
            @Override
            public void run() {
                mAdapter = new FavrouriteAdapter(getContext(), modelsList,
                        mFavPresenter);
                mRecycleView.setAdapter(mAdapter);
            }
        });
    }

    @Override
    public void onFavViewUpdate(String url, String favId, String subId) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(getContext()).load(url).into(mIconFav);
            mBtnDelete.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(favId)) {
            mTextFavId.setText(favId);
        }
        if (!TextUtils.isEmpty(subId)) {
            mTextSubId.setText(subId);
        }
    }

    @Override
    public void showDeleteMsg(String msg) {
        mHadler.post(new Runnable() {
            @Override
            public void run() {
                ToastUtil.showToast(msg);
            }
        });
    }
}
