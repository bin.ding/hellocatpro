package com.example.hellocat.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.hellocat.R;

public class EnterActivity extends BaseActivity {

    private TextView mBreedText;
    private TextView mVoteText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBreedText = (TextView) findViewById(R.id.text_breeds_lsit);
        mVoteText = (TextView) findViewById(R.id.text_votes);

        mBreedText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EnterActivity.this, BreedsListActivity.class);
                startActivity(intent);
            }
        });

        mVoteText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EnterActivity.this, VoteAndFavouriteActivity.class);
                startActivity(intent);
            }
        });
    }
}
