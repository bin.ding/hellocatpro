package com.example.hellocat.ui.view;

import com.example.hellocat.model.FavrouiteModel;

import java.util.ArrayList;

public interface IFavrouteView {

    public void onImgUpdate(ArrayList<FavrouiteModel> imgList);
    public void onFavViewUpdate(String url, String favId, String subId);

    public void showDeleteMsg(String msg);
}
