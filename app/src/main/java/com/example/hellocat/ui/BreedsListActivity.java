package com.example.hellocat.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.hellocat.R;
import com.example.hellocat.presenter.CatBreedsPresenter;
import com.example.hellocat.service.log.LogUtils;
import com.example.hellocat.ui.view.IBreedsView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class BreedsListActivity extends BaseActivity implements IBreedsView {

    private CatBreedsPresenter mCatPresenter;
    private Spinner mSpName;
    private ViewPager mViewPager;
    private TextView mDespText;
    private TextView mUrlText;
    private List<ImageView> mImgList = new ArrayList<ImageView>();
    private LinearLayout mDotLayout;
    private Handler mHandler = new Handler(Looper.myLooper());

    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breeds_list);
        mSpName = (Spinner) findViewById(R.id.sp_name);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mDespText = (TextView) findViewById(R.id.text_desp);
        mUrlText = (TextView) findViewById(R.id.text_url);
        mDotLayout = (LinearLayout) findViewById(R.id.linear_layout);
        mSpName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ++count;
                if (count <= 1) {
                    return;
                }
                mCatPresenter.getSelectData(position);
                mViewPagerDotNum = 0;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mCatPresenter = new CatBreedsPresenter(this);
        mCatPresenter.getData();
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                mDotLayout.getChildAt(mViewPagerDotNum).setEnabled(false);
                mDotLayout.getChildAt(position).setEnabled(true);
                mViewPagerDotNum = position;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private int mViewPagerDotNum = 0;

    PagerAdapter pagerAdapter = new PagerAdapter() {
        @Override
        public int getCount() {
            return mImgList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }
        @Override
        public void destroyItem(ViewGroup container, int position,
                                Object object) {
            // TODO Auto-generated method stub
            //super.destroyItem(container, position, object);
            if (position < mImgList.size()) {
                container.removeView(mImgList.get(position));
            }
        }
        //对显示的资源进行初始化
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // TODO Auto-generated method stub
            //return super.instantiateItem(container, position);
            container.addView(mImgList.get(position));
            return mImgList.get(position);
        }

    };


    private void initDot(int size) {
        //设置图片
        ImageView imageView;
        View view;
        mDotLayout.removeAllViews();
        for (int i = 0; i < size; i++) {
            //创建底部指示器(小圆点)
            view = new View(BreedsListActivity.this);
            view.setBackgroundResource(R.drawable.view_pager_background);
            view.setEnabled(false);
            //设置宽高
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(30, 30);
            //设置间隔
            if (i!= 0) {
                layoutParams.leftMargin = 10;
            }
            //添加到LinearLayout
            mDotLayout.addView(view, layoutParams);
            mDotLayout.getChildAt(0).setEnabled(true);
        }
    }

    @Override
    public void updateCatInfo(List<String> imgList, String desp, String wikiUrl) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mImgList.clear();
                for (String url : imgList) {
                    ImageView view = new ImageView(BreedsListActivity.this);
                    Picasso.with(BreedsListActivity.this).load(url).into(view);
                    mImgList.add(view);
                }
                LogUtils.d("Breeds", "Breeds updateCatInfo " + imgList.size());
                mViewPager.setAdapter(pagerAdapter);
                initDot(mImgList.size());
//                if (!TextUtils.isEmpty(imgUrl)) {
//                    Picasso.with(BreedsListActivity.this).load(imgUrl).into(mViewPager);
//                }
                if (!TextUtils.isEmpty(desp)) {
                    mDespText.setText(desp);
                }
                if (!TextUtils.isEmpty(wikiUrl)) {
                    mUrlText.setText(wikiUrl);
                }
            }
        });
    }

    @Override
    public void updateSpList(List<String> nameList, String desp, String wikiUrl) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                SpinnerAdapter adapter = new ArrayAdapter<String>(BreedsListActivity.this,
                        android.R.layout.simple_spinner_item, nameList);
                mSpName.setAdapter(adapter);
                mSpName.setSelection(0);
                if (!TextUtils.isEmpty(desp)) {
                    mDespText.setText(desp);
                }
                if (!TextUtils.isEmpty(wikiUrl)) {
                    mUrlText.setText(wikiUrl);
                }
            }
        });
    }
}
