package com.example.hellocat.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.hellocat.R;
import com.example.hellocat.model.FavrouiteModel;
import com.example.hellocat.model.FavrouriteCatModle;
import com.example.hellocat.presenter.FavrouritePresenter;
import com.example.hellocat.ui.view.IFavrouteView;
import com.squareup.picasso.Picasso;

public class FavouriteView extends LinearLayout{


    public FavrouritePresenter mPresenter;
    private ImageView mIconImg;
    private Context mContext;
    private Button mBtnFavourite;

    public FavouriteView(Context context) {
        super(context);
         mContext = context;
        View view = LayoutInflater.from(context).inflate(R.layout.favrouite_item_layout, null);
        mIconImg = (ImageView) view.findViewById(R.id.icon_fav_img);
        mBtnFavourite = (Button) view.findViewById(R.id.btn_favourite);
        addView(view);
        mBtnFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavrouiteModel model = (FavrouiteModel) FavouriteView.this.getTag();
                mPresenter.favrouiteIt(model);
            }
        });
    }

    public void updateView(FavrouiteModel modle) {
        if (modle == null) {
            return;
        }
        setTag(modle);
        String url = modle.url;
        if (TextUtils.isEmpty(url)) {
            return;
        }
        Picasso.with(mContext).load(url).into(mIconImg);
    }
}
