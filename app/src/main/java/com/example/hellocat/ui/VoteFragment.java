package com.example.hellocat.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hellocat.R;
import com.example.hellocat.model.VoteCatModel;
import com.example.hellocat.model.VoteDataModel;
import com.example.hellocat.presenter.VotePresenter;
import com.example.hellocat.service.notify.ToastUtil;
import com.example.hellocat.ui.view.IVoteView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class VoteFragment extends BaseFragment implements IVoteView {

    private ImageView mIconImg;
    private Button mBtnVote;
    private Button mBtnNope;
    private TextView mTextVote;

    private VoteDataModel mDataModel;

    private VotePresenter mPresenter;
    private Handler mHandler = new Handler(Looper.myLooper());

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_vote_layout, container, false);
        mIconImg = (ImageView) root.findViewById(R.id.icon_img);
        mBtnVote = (Button) root.findViewById(R.id.btn_vote);
        mBtnNope = (Button) root.findViewById(R.id.btn_nope);
        mTextVote = (TextView) root.findViewById(R.id.text_area);

        mBtnVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.voteCat(mDataModel);
            }
        });

        mBtnNope.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.nopeCat(mDataModel);
            }
        });

        mPresenter = new VotePresenter(this);
        mPresenter.getCat();
        return root;
    }

    @Override
    public void updateCat(VoteDataModel model) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (model == null) {
                    return;
                }
                mDataModel = model;
                if (!TextUtils.isEmpty(model.url)) {
                    Picasso.with(getContext()).load(model.url).into(mIconImg);
                }
            }
        });
    }

    @Override
    public void updateVoteArea(ArrayList<VoteDataModel> catModels) {

        if (catModels == null || catModels.size() < 0) {
            return;
        }
        StringBuffer buffer = new StringBuffer();
        for (VoteDataModel model : catModels) {
            buffer.append("1:" + model.mValue).append(" 2:" + model.mImgId)
                    .append(" 3:" + model.mCreateTime).append("\n");
        }
        if (buffer.length() > 0) {
            mTextVote.setText(buffer.toString());
        }
    }

    @Override
    public void showErrorToast(String msg) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                ToastUtil.showToast(msg);
            }
        });
    }
}
