package com.example.hellocat.service.cache;

import android.text.TextUtils;

import com.example.hellocat.model.Cat;
import com.example.hellocat.service.AppContext;
import com.example.hellocat.service.dao.DataManagerHelper;

import java.util.ArrayList;
import java.util.List;

public class CacheManager {
    public ArrayList<Cat> getCacheBreeds() {
        return (ArrayList<Cat>) new DataManagerHelper(AppContext.getContext()).getAllData();
    }

    public String getCacheUrl(String name) {
        return null;
    }

    public List<Cat> getCatByName(String name) {
        if (!TextUtils.isEmpty(name)) {
            ArrayList<Cat> list = (ArrayList<Cat>) new DataManagerHelper(AppContext.getContext()).getAllData();
            if (list != null && list.size() > 0) {
                ArrayList<Cat> result = new ArrayList<Cat>();
                for (Cat cat : list) {
                    if (cat != null && cat.name.startsWith(name)) {
                        result.add(cat);
                    }
                }
                return result;
            }
        }
        return null;
    }

    public void cacheCatBreedsData(ArrayList<Cat> list) {
        if (list == null || list.size() < 0) {
            return;
        }
        DataManagerHelper helper = new DataManagerHelper(AppContext.getContext());
        for (Cat cat : list) {
            helper.insertData(cat);
        }
    }
}
