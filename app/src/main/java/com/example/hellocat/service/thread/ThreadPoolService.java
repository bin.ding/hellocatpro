package com.example.hellocat.service.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolService {
    private static ThreadPoolService sInstace = null;

    private ExecutorService mExecutor;

    private ThreadPoolService() {
        mExecutor = Executors.newFixedThreadPool(2);
    }

    public synchronized static ThreadPoolService getInstance() {
        if (sInstace == null) {
            sInstace = new ThreadPoolService();
        }
        return sInstace;
    }

    public ExecutorService getExecutor() {
        return mExecutor;
    }
}
