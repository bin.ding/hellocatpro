package com.example.hellocat.service.network;

import com.example.hellocat.service.log.LogUtils;
import com.example.hellocat.service.thread.ThreadPoolService;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class NetworkUtils {

    public static class NetworkModel {
        public String result = null;
        public int ret = -1;
        public String json = null;
    }

    public interface INetworkCallback {
        public void onResponse(int ret, String result, Object obj);
    }

    private static final String TAG = NetworkUtils.class.getSimpleName();

    /**
     * get data
     * @param address
     * @param model
     */
    public static void getDataSync(String address, NetworkModel model) {
        CountDownLatch latch = new CountDownLatch(1);
        Runnable getTask = new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient();
                    LogUtils.d(TAG, "enter");

                    Request.Builder builder = new Request.Builder().url(address);
                    builder.addHeader("X-Api-Key", "DEMO-API-KEY");
                    if (model.json != null) {
                        RequestBody requestBody = RequestBody.create(MediaType
                                .parse("application/json; charset=utf-8"), model.json);
                        builder.post(requestBody);
                    }
                    Request request = builder.build();
                    Response response = null;
                    response = client.newCall(request).execute();
                    LogUtils.d(TAG, "finish " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        model.ret = 0;
                        model.result = response.body().string();
                        LogUtils.d(TAG, "success getDataSync " + model.result);
                    } else {
                        model.ret = -1;
                        model.result = response.body().string();
                        LogUtils.d(TAG, "[getDataSync] error " + response.message() + ",," + model.result);
                    }
                } catch (IOException e) {
                    model.ret = -1;
                    LogUtils.d(TAG, e.toString());
                } catch (Throwable t) {
                    model.ret = -3;
                    LogUtils.d(TAG, t.toString());
                } finally {
                    latch.countDown();
                }
            }
        };
        ThreadPoolService.getInstance().getExecutor().submit(getTask);
        try {
            latch.await(3000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            LogUtils.e(TAG, "count down " + e.toString());
        }
    }


    /**
     * send request async
     * @param address
     * @param callback
     */
    public static void sendRequestWithOkHttp(String address, INetworkCallback callback) {
        if (callback == null) {
            return;
        }
        LogUtils.d(TAG, "[sendRequestWithOkHttp] Breeds " + address);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(address).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onResponse(-1, null, null);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    callback.onResponse(0, response.body().string(), null);
                } else {
                    callback.onResponse(-1, null,null);
                }
            }
        });
    }

    public static void doDelete(String address, INetworkCallback callback) {
        if (callback == null) {
            return;
        }
        OkHttpClient client = new OkHttpClient();
        FormBody formBody = new FormBody.Builder().build();
        Request.Builder builder = new Request.Builder().url(address).delete(formBody);

        builder.addHeader("X-Api-Key", "DEMO-API-KEY");
        Request request = builder.build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                callback.onResponse(response.isSuccessful() ? 0 : -1, response.body().string(), null);

            }
        });
    }
}
