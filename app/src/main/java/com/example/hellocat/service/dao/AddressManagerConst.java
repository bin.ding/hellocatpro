package com.example.hellocat.service.dao;

public class AddressManagerConst {
    public static final String CAT_BREEDS_LIST_ADDR = "https://api.thecatapi.com/v1/breeds";

    public static final String CAT_BREEDS_IMG_ADDR = "https://api.thecatapi.com/v1/images/search?q=";

    public static final String MORE_KITTIES_ADDR = "https://api.thecatapi.com/v1/images/search?&limit=3";

    public static final String FAVROURITE_KITTEY_ADDR = "https://api.thecatapi.com/v1/favourites";

    public static final String FAVROUITE_DELETE_ADDR = "https://api.thecatapi.com/v1/favourites/";

    public static final String VOTE_CAT_ADDR = "https://api.thecatapi.com/v1/votes";

    public static final String VOTED_GET_ADDR = "https://api.thecatapi.com/v1/votes?";
}
