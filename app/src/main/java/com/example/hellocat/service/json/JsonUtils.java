package com.example.hellocat.service.json;

import com.example.hellocat.model.Cat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class JsonUtils {

    public static ArrayList<Cat> tranformToCatList(String json) {
        Gson gson = new Gson();
        ArrayList<Cat> catList = gson.fromJson(json, new TypeToken<List<Cat>>(){}.getType());
        return catList;
    }
}
