package com.example.hellocat.service.dao;

import android.content.Context;
import android.util.Log;

import com.example.hellocat.model.Cat;
import com.example.hellocat.service.dao.gen.CatDao;
import com.example.hellocat.service.dao.gen.DaoMaster;
import com.example.hellocat.service.dao.gen.DaoSession;

import java.util.List;

public class DataManagerHelper {
    private static final String DATABASE_NAME = "cat.db";
    private static final String TAG = DataManagerHelper.class.getSimpleName();
    private DaoSession mDaoSession;

    public DataManagerHelper(Context context) {
        initDaoSesion(context);
    }

    private void initDaoSesion(Context mContext) {
        DaoMaster.OpenHelper openHelper = new DaoMaster.DevOpenHelper(
                mContext.getApplicationContext(), DATABASE_NAME, null);
        DaoMaster daoMaster = new DaoMaster(openHelper.getWritableDatabase());
        mDaoSession = daoMaster.newSession();
    }

    public void insertData(Cat cat) {
        CatDao catDao = mDaoSession.getCatDao();
        catDao.insertOrReplace(cat);
    }

    public void deleteData(String id) {
//        CatDao catDao = mDaoSession.getCatDao();
//        catDao.deleteByKey(id);
    }

    public void updateData(long id) {
        Log.d(TAG, "updateData: id: " + id);
        CatDao catDao = mDaoSession.getCatDao();
        Cat cat = catDao.queryBuilder()
                .where(CatDao.Properties.Id.eq(id))
                .build()
                .unique();
        catDao.update(cat);
    }

    public List<Cat> getAllData() {
        CatDao catDao = mDaoSession.getCatDao();
        List<Cat> catList = catDao.queryBuilder().orderAsc(CatDao.Properties.Id).build().list();
        return catList;
    }
}
