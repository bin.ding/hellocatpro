package com.example.hellocat.service.notify;

import android.widget.Toast;

import com.example.hellocat.service.AppContext;

public class ToastUtil {

    public static void showToast(String msg) {
        Toast.makeText(AppContext.getContext(), msg, Toast.LENGTH_LONG).show();
    }
}
